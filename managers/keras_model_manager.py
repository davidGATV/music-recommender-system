import os
import numpy as np
from helper.settings import (logger, test_size, dev_size,
                             mandatory_shape, model_name, optimizer_name,
                             batch_size, epochs, n_channels)
from sklearn.model_selection import train_test_split
from helper.utils import (prepare_directory, write_json_file, read_json_file)
from typing import Optional
from keras import optimizers
from generators.audio_batch_generator import AEDataGenerator
from keras.models import Model, load_model
from keras.metrics import MSE, MAE
from keras.losses import binary_crossentropy
from keras.layers import (Input, Conv2D, Cropping2D, MaxPooling2D,
                          UpSampling2D, ZeroPadding2D,Flatten, Reshape,
                          LeakyReLU, BatchNormalization)
from keras.callbacks import History


class ModelManager:
    def __init__(self, features_dim: dict, window: bool = True,
                 window_size: int = 6):
        self.features_dim: dict = features_dim
        self.test_size = test_size
        self.dev_size = dev_size
        self.mandatory_shape = mandatory_shape
        self.model_name: str = model_name
        self.mandatory_shape: int = mandatory_shape
        self.metrics: dict = self.get_custom_metrics()
        self.losses: dict = self.get_custom_losses()
        self.optimizer_name: str = optimizer_name
        self.batch_size: int = batch_size
        self.epochs: int = epochs
        self.n_channels: int = n_channels
        self.window: bool = window
        self.window_size: int = window_size

    def generate_train_dev_test_sets(self, all_docs_ids: list):
        train_ids: list = []
        dev_ids: list = []
        test_ids: list = []
        try:
            # Divide document ids into train and test
            train_ids, test_ids = train_test_split(all_docs_ids, test_size=self.test_size)

            train_ids, dev_ids = train_test_split(train_ids, test_size=self.dev_size)
        except Exception as e:
            logger.error(e)
        return train_ids, dev_ids, test_ids

    @staticmethod
    def get_custom_metrics():
        custom_metrics: dict = {"mse": MSE, "mae": MAE}
        return custom_metrics

    @staticmethod
    def get_custom_losses():
        custom_losses: dict = {"mse": MSE,
                               "binary_crossentropy": binary_crossentropy}
        return custom_losses

    def build_music_autoencoder(self):
        autoencoder: Optional[Model] = None
        try:
            autoencoder: Model = self.build_autoencoder_system(
                features_dim=self.features_dim,
                model_name=self.model_name,
                mandatory_shape=self.mandatory_shape)
        except Exception as e:
            logger.error(e)
        return autoencoder

    @staticmethod
    def build_encoder_branch(inputs, encoder_layer_name: str,
                             mandatory_shape: int,
                             last_layer_name: str):
        encoder: Optional[Flatten] = None
        try:
            # Check input
            if inputs.get_shape()[2] != mandatory_shape:
                crop_val: tuple = (np.abs(int(mandatory_shape - inputs.get_shape()[2])), 0)
            else:
                crop_val: tuple = (0, 0)

            x = Cropping2D(cropping=((0, 0), crop_val), data_format="channels_last")(inputs)
            x = BatchNormalization()(x)
            x = Conv2D(64, (3, 3), padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = MaxPooling2D((2, 2), padding='same')(x)
            x = Conv2D(32, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = MaxPooling2D((2, 2), padding='same')(x)
            x = Conv2D(32, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = MaxPooling2D((2, 2), padding='same')(x)
            x = Conv2D(16, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = MaxPooling2D((2, 2), padding='same', name=last_layer_name)(x)
            encoder: Flatten = Flatten(name=encoder_layer_name)(x)
        except Exception as e:
            logger.error(e)
        return encoder

    @staticmethod
    def build_decoder_branch(input_encoded: Flatten,
                             decoder_layer_name: str,
                             reshaped_input: tuple,
                             crop_up_sampling: bool = False):
        decoder: Optional[Cropping2D] = None
        try:
            x = Reshape(reshaped_input)(input_encoded)
            x = Conv2D(16, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = UpSampling2D((2, 2))(x)
            x = Conv2D(32, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = UpSampling2D((2, 2))(x)
            if crop_up_sampling:
                x = Cropping2D(cropping=((1, 0), (0, 0)), data_format="channels_last")(x)
            x = Conv2D(32, (3, 3),  padding='same')(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = UpSampling2D((2, 2))(x)
            x = Conv2D(64, (3, 3),  padding="same")(x)
            x = LeakyReLU(alpha=0.1)(x)
            x = UpSampling2D((2, 2))(x)
            x = Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)
            x = ZeroPadding2D(padding=(0, 3), data_format="channels_last")(x)
            decoder: Cropping2D = Cropping2D(cropping=((0, 0), (1, 0)),
                                             data_format="channels_last",
                                             name=decoder_layer_name)(x)
        except Exception as e:
            logger.error(e)
        return decoder

    @staticmethod
    def build_autoencoder_system(features_dim: dict, model_name: str, mandatory_shape: int):
        autoencoder: Optional[Model] = None
        try:
            # Initialise inputs and outputs
            inputs: list = []
            outputs: list = []

            crop_up_sampling: bool = False
            # Build Model
            for name, dim in features_dim.items():
                # 1. Create inputs
                input_nn: Input = Input(dim + (1,))
                inputs.append(input_nn)

                # 2. Create encoder
                encoder: Flatten = __class__.build_encoder_branch(
                    inputs=input_nn,
                    encoder_layer_name=f"encoder_{name}",
                    last_layer_name=f"last_layer_{name}",
                    mandatory_shape=mandatory_shape)

                # 3. Get shape of the last layer to generate Decoder
                encoder_shape: tuple = __class__.extract_output_shape_from_encoder(
                    input_data=input_nn,
                    encoder_output=encoder,
                    last_layer_name=f"last_layer_{name}")

                # Special case Chroma
                if name == "chroma":
                    crop_up_sampling = True

                # 4. Build Decoder branch
                decoder = __class__.build_decoder_branch(
                    input_encoded=encoder,
                    decoder_layer_name=f"decoder_{name}",
                    reshaped_input=encoder_shape,
                    crop_up_sampling=crop_up_sampling)
                # 5. Save decoder as output
                outputs.append(decoder)

            # Build autoencoder
            autoencoder = Model(inputs=inputs,
                                outputs=outputs,
                                name=model_name)
            # Show summary
            autoencoder.summary()
        except Exception as e:
            logger.error(e)
        return autoencoder

    @staticmethod
    def extract_output_shape_from_encoder(input_data: Input, encoder_output: Flatten,
                                          last_layer_name: str):
        encoder_output_shape: tuple = ()
        try:
            encoder_model: Model = Model(input_data, encoder_output)
            encoder_output_shape: tuple = encoder_model.get_layer(last_layer_name).output_shape[1:]
        except Exception as e:
            logger.error(e)
        return encoder_output_shape

    @staticmethod
    def select_optimizer(opt_name="adam"):
        optimizer: Optional[optimizers] = None
        try:
            if opt_name == 'sgd':
                optimizer = optimizers.SGD()
            elif opt_name == 'adagrad':
                optimizer = optimizers.Adagrad()
            elif opt_name == 'adadelta':
                optimizer = optimizers.Adadelta()
            elif opt_name == 'adam':
                optimizer = optimizers.Adam()
            elif opt_name == 'nadam':
                optimizer = optimizers.Nadam()
        except Exception as e:
            logger.error(e)
        return optimizer

    def compile_autoencoder(self, autoencoder: Model, loss_name: str = "mse",
                            metric_name: str = "mae"):
        try:
            optimizer: optimizers = self.select_optimizer(opt_name=self.optimizer_name)
            n_inputs: int = len(autoencoder.inputs)
            loss: list = [self.losses.get(loss_name) for i in range(n_inputs)]
            metrics: list = [self.metrics.get(metric_name)]
            autoencoder.compile(optimizer=optimizer,
                                loss=loss,
                                metrics=metrics)
        except Exception as e:
            logger.error(e)
        return autoencoder

    def train_autoencoder_generator(self, autoencoder: Model,
                                    train_ids: list, dev_ids: list):
        autoencoder_history: Optional[History] = None
        try:
            steps_per_epoch = int(len(train_ids) / self.batch_size)
            validation_steps = int(len(dev_ids) / self.batch_size)

            """
            docs_vocab: dict, batch_size: int,
                 window: bool = True, window_size: int = 6,
                 shuffle: bool = True, to_fit: bool = True,
                 channels: int = 1, features: list = None
            """
            train_docs_vocab: dict = {k: v for k, v in enumerate(train_ids)}
            dev_docs_vocab: dict = {k: v for k, v in enumerate(dev_ids)}

            features: list = list(self.features_dim.keys())

            # Create Train || Validation Generators
            train_generator = AEDataGenerator(
                docs_vocab=train_docs_vocab,
                window=self.window,
                window_size=self.window_size,
                features=features,
                channels=self.n_channels,
                batch_size=self.batch_size,
                shuffle=True, to_fit=True)

            validation_generator = AEDataGenerator(
                docs_vocab=dev_docs_vocab,
                window=self.window,
                window_size=self.window_size,
                features=features,
                channels=self.n_channels,
                batch_size=self.batch_size,
                shuffle=True, to_fit=True)

            autoencoder_history: History = autoencoder.fit(train_generator,
                                                                     validation_data=validation_generator,
                                                                     steps_per_epoch=steps_per_epoch,
                                                                     validation_steps=validation_steps,
                                                                     epochs=self.epochs)
        except Exception as e:
            logger.error(e)

        return autoencoder_history

    @staticmethod
    def save_history(history: History, history_directory: str, history_name: str):
        try:
            # Check if directory exists
            prepare_directory(directory=history_directory)
            history_path = os.path.join(history_directory, history_name)
            # Write JSON
            write_json_file(data=history.history, filename=history_path)
        except Exception as e:
            logger.error(e)

    @staticmethod
    def load_history(history_directory: str, history_name: str):
        model_history: Optional[History] = None
        try:
            history_path: str = os.path.join(history_directory, history_name)
            # Load model history
            model_history: History = read_json_file(history_path)
        except Exception as e:
            logger.error(e)
        return model_history

    @staticmethod
    def load_trained_model(model_directory: str, model_name: str):
        model: Optional[Model] = None
        try:
            model_path: str = os.path.join(model_directory, model_name)
            # Load model
            model: Model = load_model(model_path)
        except Exception as e:
            logger.error(e)
        return model

    @staticmethod
    def save_trained_model(model: Model, model_directory: str, model_name: str, extension: str = ".h5"):
        try:
            # Check if directory exists
            prepare_directory(directory=model_directory)
            # Check if extension is not included in the name of the model
            if len(model_name.split(".")) <= 1:
                model_name += extension
            # Merge path
            model_path: str = os.path.join(model_directory, model_name)
            # Save model
            model.save(model_path)
        except Exception as e:
            logger.error(e)

    @staticmethod
    def load_encoder_model(model_directory: str, model_name: str, encoder_mel: str,
                           encoder_chroma: str):
        encoder: Optional[Model] = None
        try:
            model_path = os.path.join(model_directory, model_name)
            # Load model
            model = load_model(model_path)
            encoder: Model = Model(inputs=model.inputs,
                                   outputs=[model.get_layer(encoder_mel).output,
                                            model.get_layer(encoder_chroma).output])
        except Exception as e:
            logger.error(e)
        return encoder

    @staticmethod
    def get_encoder_size_by_name(model: Model, layer_name: str):
        encoder_size: int = -1
        try:
            encoder_size: int = int(model.get_layer(layer_name).output_shape[1])
        except Exception as e:
            logger.error(e)
        return encoder_size

    @staticmethod
    def generate_embeddings(model: Model, mel_data: list, chroma_data: list,
                            mel_dim: tuple, chroma_dim: tuple):

        x_all_embedding: np.array = np.array([])
        try:
            x_mel = np.array(mel_data).reshape(mel_dim)
            x_chroma = np.array(chroma_data).reshape(chroma_dim)

            x_embedding: list = model.predict([x_mel, x_chroma])
            x_all_embedding: np.array = np.concatenate([i for i in x_embedding], axis=1)
        except Exception as e:
            logger.error(e)
        return x_all_embedding
