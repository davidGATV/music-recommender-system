from helper.utils import get_files_from_directory
from helper.settings import logger
from musfeat.musfeat import MusFeat
from musfeat.models.audio_models import AudioAnalysisFeature
import numpy as np
import os

dirname: str = "D:\\DAVID\\Datasets\\SpotTrack101"
file_list = get_files_from_directory(dirname)

# Index vocabulary
audio_index_vocab: dict = {k: v for k, v in enumerate(file_list)}

# Generate Batch of m samples
window: bool = True
seconds_per_analysis: int = 6
total_batch_samples: int = 128
m: int = int(total_batch_samples/6)

features = ["mel", "chroma"]
x_features: dict = {k: [] for k in features}
feature_dims: dict = {k: () for k in features}
musfeat_obj: MusFeat = MusFeat()


def preprocess_feature(feature_obj: list, feature_dim: tuple):
    x: np.ndarray = np.array([])
    try:
        rng = np.random.default_rng()
        x: np.ndarray = np.array(feature_obj).reshape((-1,) + feature_dim)
        # Shuffle object
        rng.shuffle(x=x, axis=0)
        # Expand dimension
        x: np.ndarray = np.expand_dims(x, axis=(len(feature_dim)+1))
    except Exception as e:
        logger.error(e)
    return x


# ===========================================================
for i in range(3):
    file = file_list[i]
    print(i)
    print(file)
    # Audio Path Parameters
    audio_file_path: str = os.path.join(dirname, file)
    # Default parameters
    audio_features: AudioAnalysisFeature = musfeat_obj.get_naive_audio_features_from_file(
        audio_file_path=audio_file_path, window=window,
        seconds_per_analysis=seconds_per_analysis)

    for feature in x_features.keys():
        x_features[feature] += [i for i in audio_features.__getattribute__(feature)]
        feature_dims[feature] = audio_features.__getattribute__(f"{feature}_size")

for feature in x_features.keys():

    x_feature: np.ndarray = preprocess_feature(
        feature_obj=x_features.get(feature),
        feature_dim=feature_dims.get(feature))
    x_features[feature] = x_feature
# ===========================================================


