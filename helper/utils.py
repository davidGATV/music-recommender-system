import os, json
from helper.settings import logger
from uuid import uuid4


def get_files_from_directory(directory: str):
    file_list: list = []
    try:
        for dirname, _, filenames in os.walk(directory):
            file_list += [ filename for filename in filenames]
    except Exception as e:
        logger.error(e)
    return file_list


def generate_uuid():
    event_id: str = str(uuid4())
    return event_id


def prepare_directory(directory: str):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except Exception as e:
        logger.error(e)


def write_json_file(data: dict, filename: str):
    try:
        json_data = json.dumps(str(data))
        with open(filename, 'w') as file:
            json.dump(json_data, file)
    except Exception as e:
        logger.error(e)


def read_json_file(filename):
    data = None
    try:
        with open(filename, 'r') as file:
            data = json.load(file)
    except Exception as e:
        logger.error(e)
    return data