import coloredlogs, logging
import warnings, os
warnings.filterwarnings('ignore')

logger = logging.getLogger(__name__)
coloredlogs.install(level='DEBUG', logger=logger)

# Model Settings
test_size: float = 0.2
dev_size: float = 0.3
mandatory_shape: int = 512
model_name: str = os.getenv("AE_NAME") if "AE_NAME" in os.environ else "MUSIC-AENET"
optimizer_name: str = "adam"
batch_size: int = 32
epochs: int = 1
n_channels: int = 1




