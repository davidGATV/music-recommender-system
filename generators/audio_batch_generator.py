from helper.settings import logger
from keras.utils import Sequence
from musfeat.musfeat import MusFeat
from musfeat.models.audio_models import AudioAnalysisFeature
import numpy as np


class AEDataGenerator(Sequence):
    """Generates data for Keras
    Sequence based data generator. Suitable for building data generator for training and prediction.
    """
    def __init__(self, docs_vocab: dict, batch_size: int,
                 window: bool = True, window_size: int = 6,
                 shuffle: bool = True, to_fit: bool = True,
                 channels: int = 1, features: list = None):

        self.docs_vocab: dict = docs_vocab
        self.indexes: list = list(docs_vocab.keys())
        self.batch_size: int = batch_size
        self.window: bool = window
        self.window_size: int = window_size
        self.shuffle = shuffle
        self.to_fit: bool = to_fit
        self.channels: int = channels
        self.features: list = features
        self.x_features: dict = {k: [] for k in features}
        self.feature_dims: dict = {k: () for k in features}

        self.musfeat_obj: MusFeat = MusFeat()
        self.on_epoch_end()

    def __len__(self):
        """Denotes the number of batches per epoch
        :return: number of batches per epoch
        """
        return int(np.floor(len(self.indexes) / self.batch_size))

    def __getitem__(self, index):
        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Get list of documents
        docs_files: list = [self.docs_vocab.get(k) for k in indexes]
        # Generate data
        x: list = self.generate_x(docs_files)
        if self.to_fit:
            return x, x
        else:
            return x

    def on_epoch_end(self):
        """Updates indexes after each epoch
        """
        self.indexes: np.arange = np.arange(len(self.indexes))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def generate_x(self, docs_files: list):

        # 1. Loop over the list of filepaths
        self.x_features: dict = {k: [] for k in self.features}
        self.prepare_batch_data(docs_files=docs_files)
        # Return X
        x: list = [*self.x_features.values()]
        return x

    def prepare_batch_data(self, docs_files: list):
        try:
            # NUmber of files needed
            m: int = 0

            # 1. Loop over the list of filepaths
            for i, filepath in enumerate(docs_files):
                if m < self.batch_size:
                    # 1. Extract Features
                    audio_features: AudioAnalysisFeature = self.musfeat_obj.get_naive_audio_features_from_file(
                        audio_file_path=filepath, window=self.window,
                        seconds_per_analysis=self.window_size)

                    # 2. Store both features and their dimensions into a dictionary
                    for feature in self.x_features.keys():
                        # Make Sure we retrieve the batch size elements
                        if m < self.batch_size:
                            self.x_features[feature] += [i for i in audio_features.__getattribute__(feature)]
                            self.feature_dims[feature] = audio_features.__getattribute__(f"{feature}_size")
                    # Update m
                    m += audio_features.n_segments

            # 3. Preprocess features by converting them into numpy arrays
            for feature in self.x_features.keys():
                x_feature: np.ndarray = self.preprocess_feature(
                    feature_obj=self.x_features.get(feature),
                    feature_dim=self.feature_dims.get(feature),
                    batch_size=self.batch_size)
                # print(f"feature {feature} shape: {x_feature.shape}")

                self.x_features[feature] = x_feature

        except Exception as e:
            logger.error(e)

    @staticmethod
    def preprocess_feature(feature_obj: list, feature_dim: tuple, batch_size: int):
        x: np.ndarray = np.array([])
        try:
            rng = np.random.default_rng()
            x: np.ndarray = np.array(feature_obj).reshape((-1,) + feature_dim)
            # Shuffle object
            rng.shuffle(x=x, axis=0)
            # Expand dimension
            x: np.ndarray = np.expand_dims(x, axis=(len(feature_dim) + 1)).astype('float32')

            # Normalized
            x = x / np.linalg.norm(x)

            # Retrieve Batch size rows
            x = x[:batch_size, ]
        except Exception as e:
            logger.error(e)
        return x