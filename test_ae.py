from helper.utils import get_files_from_directory
from managers.keras_model_manager import ModelManager
from keras.models import Model
from keras.callbacks import History
from musfeat.musfeat import MusFeat
from musfeat.models.audio_models import NaiveFeaturesDim
from helper.settings import model_name
from helper.utils import generate_uuid
import os

window: bool = True
window_size: int = 6

# Change path
# ===================================================
dirname: str = "D:\\DAVID\\Datasets\\SpotTrack101"
# ===================================================

model_uuid: str = generate_uuid()
model_directory: str = os.path.join("trained_models", model_uuid)
history_directory: str = model_directory
history_name: str = model_name + "_history.json"


musfeat_obj: MusFeat = MusFeat()
feature_dims: NaiveFeaturesDim = musfeat_obj.get_naive_features_dim(
    seconds_per_analysis=window_size)

all_docs_ids: list = [os.path.join(dirname, i) for i in get_files_from_directory(dirname)]


# Initialise model object
model_manager: ModelManager = ModelManager(
    features_dim=feature_dims.__dict__,
    window=window,
    window_size=window_size)

# Generate sets
train_ids, dev_ids, test_ids = model_manager.generate_train_dev_test_sets(
    all_docs_ids=all_docs_ids)

# Build autoencoder
autoencoder: Model = model_manager.build_music_autoencoder()

# Compile model
autoencoder: Model = model_manager.compile_autoencoder(autoencoder=autoencoder)

# Train autoencoder
autoencoder_history: History = model_manager.train_autoencoder_generator(
    autoencoder=autoencoder,
    train_ids=train_ids,
    dev_ids=dev_ids)

# Model
model_manager.save_trained_model(
    model=autoencoder,
    model_directory=model_directory,
    model_name=model_name)
# Model history
model_manager.save_history(
    history=autoencoder_history,
    history_directory=history_directory,
    history_name=history_name)
